Title:  pd-touchosc
Author: Albert Gräf <aggraef@gmail.com>
Date:   2014-03-20

pd-touchosc
===========

This software is for use with hexler's TouchOSC, a multitouch OSC controller
for computer music and other multimedia applications, which is available as a
mobile app running on Android and iOS devices. TouchOSC comes with a graphical
layout editor which lets you configure the user interface displayed on the
device according to your needs.

TouchOSC enables you to assign MIDI messages to the control elements so that
it can also be used as a MIDI controller. This is useful if you need to
interface to existing MIDI software and hardware which doesn't offer a
suitable OSC interface. To make this work, you need a special program, the
TouchOSC MIDI Bridge, on the host computer. Unfortunately, this program is
proprietary software which is only available on Mac and Windows systems right
now.

pd-touchosc is a replacement for the TouchOSC MIDI Bridge. Given a TouchOSC
layout, it translates between OSC and MIDI messages on the fly, using the MIDI
mappings defined in the layout. The software is currently implemented as a
library of Pd externals and accompanying patches, thus you need Miller
Puckette's Pd to run it. It is intended primarily for Linux and other Un*x
systems not supported by the official TouchOSC MIDI bridge, but of course you
can also use it on Mac OS X and Windows if you have the requisite software
installed (see below).

License
=======

pd-touchosc is Copyright (c) 2014 by Albert Gräf. It is distributed under the
3-clause BSD license, please check the COPYING file for details.

Installation
============

You need:

- hexler's [TouchOSC](http://hexler.net/software/touchosc). You can grab the
  mobile application on Google Play or the iTunes Store and install it on
  your Android or iOS device. The TouchOSC layout editor is available freely
  on the TouchOSC website; you'll need this to create your own TouchOSC
  layouts.

- [Pd](http://puredata.info), along with the cyclone and mrpeach externals. We
  recommend using one of the available compilations such as pd-extended or
  pd-l2ork which already includes the required externals, but you can also use
  vanilla Pd if you install the cyclone and mrpeach externals manually.

- [Pure](http://purelang.bitbucket.org/). pd-touchosc is written in the
  author's Pure programming language, so you need to have the Pure interpreter
  installed, as well as the pd-pure plugin loader and the pure-stldict and
  pure-xml modules. You can find all of these on the Pure website.

Optionally, to get Zeroconf support (needed for automatic discovery of OSC
network services), you'll also need the pure-avahi or pure-bonjour module
(pd-touchosc will work with either of these; usually you use pure-avahi on
Linux and pure-bonjour on the Mac). This is highly recommended since it makes
setting up the OSC network connections much easier. Please make sure that you
install pure-avahi or pure-bonjour *before* compiling pd-touchosc if you want
to use this.

Users of Arch Linux may want to check the Arch User Repositories (AUR), as we
try to maintain a fairly complete and recent collection of Pure- and
Pd-related packages there.

Mac OS X users can find a ready-made binary package for 64 bit Intel systems
here: [pd-touchosc-0.1-macosx-x86_64.zip](https://bitbucket.org/agraef/pd-touchosc/downloads/pd-touchosc-0.1-macosx-x86_64.zip). The zip file contains the
`touchosc` directory which you'll have to copy to a directory on Pd's library
path (usually `/Library/Pd` for system-wide and `~/Library/Pd` for personal
installation on the Mac). (You'll also need the Pure interpreter which is
available in [MacPorts](http://www.macports.org/). Please check the [Pure On
Mac OS X](https://bitbucket.org/purelang/pure-lang/wiki/PureOnMacOSX) wiki
page for details.)

To compile the software yourself, check the included Makefile for settings
that might need to be adjusted for your system, then run:

    make
    sudo make install

This works with vanilla Pd. If you're running pd-extended or pd-l2ork then
during installation you need to specify the Pd flavor using the `PD` make
variable, e.g.:

    sudo make install PD=pd-extended

Getting Started
===============

Before you can use this software inside Pd, you need to configure some things.
First, make sure that you have the cyclone and mrpeach externals installed and
on your Pd search path (`-path` option, or `Preferences/Path` inside Pd).
While you're there, you may also want to add the newly installed touchosc
directory so that Pd will be able to locate the helper patches included in the
pd-touchosc distribution. You need to specify an absolute path here, which
depends on your Pd installation (usually the externals can be found under
/usr/lib/pd/extra; replace pd with the particular Pd flavour that you have,
e.g., pd-extended or pd-l2ork).

Next, you need to make sure that the `touchosc` library, which contains the
externals, gets loaded inside Pd. You can do this either with the `-lib`
option on the command line when invoking Pd, or using the special `import`
object in your patch. However, if you're going to use pd-touchosc a lot then
it will be most convenient to just add `touchosc` to your Pd startup options
(`Media/Preferences/Startup` in Pd 0.43 and later).

The recommended way to use pd-touchosc is through the provided touchosc-bridge
helper patch which takes care of all the nitty-gritty details of connecting to
TouchOSC. This is invoked with one creation parameter, the name of the
TouchOSC layout to be used. If necessary, you can also specify the TouchOSC
UDP ports as the second and third parameter (8000 and 9000 are used by
default, which matches the TouchOSC defaults).

**Note:** The externals in the `touchosc` library look for TouchOSC layouts in
the directory of the hosting patch if you specify them using a relative
path. This will be the directory containing the touchosc-bridge patch if you
use that abstraction, *not* the directory of your main patch. Thus, if you're
working with your own TouchOSC layouts, it makes things easier if you keep
your main patch, the touchosc-bridge patch and the TouchOSC layouts that you
use all together in the same directory.

There's also a somewhat simpler version of the patch available, see the
touchosc-bridge-simple.pd file which might be preferable if you want a less
cluttered version which only has the bare bones necessary to get a working OSC
network connection. However, if you want Zeroconf support (see below) to
facilitate the connection setup then you'll need the full version of the
patch.

Of course, if you're a seasoned Pd hacker then you can just have a look at the
abstraction and modify it for your own purposes. If you know the Pure
language, you might also want to adjust the basic tomidi, toosc and oscbrowser
externals on which touchosc-bridge is built. Use the source, Luke!

Getting Connected
=================

The easiest way to get connected is through WiFi. Any kind of local network
connection between the PC running Pd and the Android or iOS device running
TouchOSC will do, but note that TouchOSC requires that some UDP ports are open
(8000 and 9000 by default) so that the OSC clients can speak to each other.
Please check the TouchOSC documentation on how to get your TouchOSC instance
connected to the host device. The latest version of pd-touchosc supports
Zeroconf via Avahi or Bonjour, so if you have the corresponding Pure module
installed, a client named `pd-touchosc` should show up in the host list in
TouchOSC's OSC configuration dialog. Click on that to have the network address
and port number filled in.

As soon as touchosc-bridge receives some OSC data from the device, it will
configure its output accordingly, so that data can be sent back to the
device. (An OSC `/ping` message suffices; TouchOSC can be configured to spit
out these messages at regular intervals.) Note that this method works best if
you stick to TouchOSC's default incoming port 9000; there's usually no reason
to change this anyway unless that port is already in use for something else.

As an alternative, touchosc-bridge also offers the option to browse for
available OSC services using Zeroconf. The patch has a toggle which lets you
enable this; it will then connect to the first OSC service available in the
network (other than `pd-touchosc` itself). If there's more than one such
service, you can cycle through the available services with the other GUI
controls of the patch. E.g., if you're running the Android version of
TouchOSC, look out for services named `Android (TouchOSC)` and pick the one
that you want.

If all this fails, you can also connect manually, by entering the PC's network
address in TouchOSC's OSC configuration. On the host side, sending a message
such as `connect 192.168.1.100 9000` with the device's network address to the
first inlet of touchosc-bridge will do the trick. But this should rarely be
necessary.

Obviously, this may require some fiddling to get the network connection to
work. If you have multiple devices and/or pd-touchosc instances running on
your network then it's a good idea to give the clients different names so that
you can easily identify them.  TouchOSC allows you to do this in the `ZeroConf
Name` field of its OSC configuration. The `pd-touchosc` service name of the
touchosc-bridge abstraction is currently hardwired into it, but you can easily
open the patch and change it there (look for the `oscbrowser` object in the
patch, it has the published OSC service name and port number as its
arguments).

Help and Documentation
======================

A help patch is available which demonstrates how to use the touchosc-bridge
abstraction. Open the Pd help browser and look for a section named `touchosc`.
You'll find a patch named `touchosc-bridge-help` there. Or just double-click
on the `touchosc` entry; in most recent Pd versions this will open your
preferred file manager on the corresponding directory where you can find the
help patch along with a bunch of other instructive examples for your perusal,
so that you can kick the tires and figure out how things work.

There's also a paper for the upcoming LAC 2014 conference included in the
distribution which describes the software in more detail, see
<https://bitbucket.org/agraef/pd-touchosc/src/master/touchosc-lac14.pdf>.
(The paper is slightly outdated already, but most of the information is still
valid.)

Feedback and Bug Reports
========================

As usual, bug reports, patches, feature requests, other comments and source
contributions are more than welcome. Just drop me an email, file an issue at
the tracker or send me a pull request on pd-touchosc's Bitbucket page
<https://bitbucket.org/agraef/pd-touchosc>.

Enjoy! :)

Albert Gräf <aggraef@gmail.com>
